# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.18.0"
  constraints = "3.18.0"
  hashes = [
    "h1:YcQw1GDEp0UVSfXweVBr69on33ypBY13kG+lTeCSSx8=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:20cbbf732def9534f03270064e79e1f955bf93c13893c6cf9c9d369bb49041ef",
    "zh:34e91542ab5ec7a1df0c3f4dba0c898ae7f55a61369300d20f536fb5c051a1e2",
    "zh:41ad3e89ecc54e6aa1a1cc71e1d9f17eac438b0d26757f55ae196a665ba78fea",
    "zh:4aa8974e99f4bd81b2ca15cc5bf73afc33e246219f4f2306a1380e619b7e3a76",
    "zh:60715337e79d9cdd442f95ca51e70405fb779cf02091f6771fa285163f270538",
    "zh:6e33fef76103c0ea255ca87db08072af1566354de63da826b73df74f6710a00f",
    "zh:78600f026ee35710aea301a45305e96aff97f1b122ef491c3214897f82d8f4a1",
    "zh:79ef39ca66539cd999b61f7bfa3a68377f7b299f23bdcbdb991d59d74df2da92",
    "zh:7d66fc368e8c5a61ef5044326f74c493cf743e240bed147bc1310b2aa0836aea",
    "zh:9055179712413c45ee696a7854aebebc3161442dfac58afe9548e672358315d6",
    "zh:e02e3679db81f668ef80cb53225ad6cb7f6c5ee9b85a202a5ea58361fa20eb3d",
    "zh:e96f1d556048d4cb80cfe0dbf5150986ff0e1d0c8a7594be5324b5aafd381ef4",
    "zh:ef9c7272f4e1f0a195dc13e8f05aa1afe0dee3fc80d73c51bac26746875993ec",
    "zh:fd226f57916b5ab51275ed6ddff74bce99398d327e1cc7fde4adb9315a9dc50f",
  ]
}

provider "registry.terraform.io/tehcyx/kind" {
  version     = "0.0.15"
  constraints = "0.0.15"
  hashes = [
    "h1:tb+xaIXpYZodyagE+Oa+XcFxE5Du2MU9k9ReNJbNius=",
    "zh:02ab0e18f0bacb67c3a223043f6988ef540e4c8b34e4c629f39c1da929dac474",
    "zh:28081186a515ca07d62331a7f0715bb0aadf892c277623851e63bd8f7038ad98",
    "zh:78c35927ce6f0b855d1eb46afc5f204dd5df91723c1033bbcd239ba1d665156b",
    "zh:7a9c7645b49e7824b126c2701ed9499adfdeeb608decd19b6da0b95206fb7037",
    "zh:9b6d258af1b6c98b9faa794e3b590517c6d7e72b59e211eea98077f7fa5953a0",
    "zh:e8d25409d271cfd4588fc02e9362e6d25c965555a07df02815778fa4c87d1855",
  ]
}
