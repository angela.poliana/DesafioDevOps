resource "gitlab_project" "repository" {
  name        = var.name
  description = var.description
  visibility_level = "public"
}