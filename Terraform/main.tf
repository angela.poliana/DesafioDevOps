module "gitlab" {
  name = "DesafioDevOps"
  description = "Repositorio do desafio final do programa Formando DevOps"
  source = "./project"
}

module "kind" {
  name = "my-cluster"
  source = "./cluster"
}

