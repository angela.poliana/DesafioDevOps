output "api_endpoint" {
  value = module.kind.api_endpoint
}

output "kubeconfig" {
  value = module.kind.kubeconfig
}

output "client_certificate" {
  value = module.kind.client_certificate
}

output "client_key" {
  value = module.kind.client_key
}

output "cluster_ca_certificate" {
  value = module.kind.cluster_ca_certificate
}
