### 1 - Gitlab

Pipeline com build da imagem do app, linter do Dockerfile e scan da Imagem usando Trivy:

[.gitlab-ci.yml](https://gitlab.com/angela.poliana/DesafioDevOps/-/blob/master/.gitlab-ci.yml)

Repositório das imagens no dockerhub:<br>
![prep01](Imagens/6.png)<br>
Link para o repositório das imagens no dockerhub:<br>
[angelapoliana/podinfo](https://hub.docker.com/repository/docker/angelapoliana/podinfo)

##### CI/CD 

[CI/CD](https://gitlab.com/angela.poliana/DesafioDevOps/-/pipelines/695748367)<br>
[(DockerLint Dockerfile)](https://gitlab.com/angela.poliana/DesafioDevOps/-/jobs/3328004821)<br>
[(Docker Build)](https://gitlab.com/angela.poliana/DesafioDevOps/-/jobs/3328004823)<br>
[(Container_Scanning)](https://gitlab.com/angela.poliana/DesafioDevOps/-/jobs/3328004825)<br>
[(Kubernetes Deployment)](https://gitlab.com/angela.poliana/DesafioDevOps/-/jobs/3328004826)<br>


### 2 - TERRAFORM 
-> Cluster kind<br>
-> Repositório no Gitlab

##### Códigos em HCL para criação do Cluster Kubernets e do Repositório do Gitlab:<br>

[Códigos Terraform](https://gitlab.com/angela.poliana/DesafioDevOps/-/tree/master/Terraform)

![prep02](Imagens/1.png)<br>

### 3 - KUBERNETES 

Código para realizar o deployment:<br>
[deployment.yaml](https://gitlab.com/angela.poliana/DesafioDevOps/-/blob/master/deployment.yaml)

```
angela@angela:~/Desktop/DesafioDevOps$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
loki-0                     1/1     Running   0          32h
loki-promtail-7ftx2        1/1     Running   0          32h
loki-promtail-7w2x5        1/1     Running   0          32h
podinfo-6cf8996bcd-xwcrz   1/1     Running   0          64s
```
```
angela@angela:~/Desktop/DesafioDevOps$ kubectl get deployments.apps 
NAME      READY   UP-TO-DATE   AVAILABLE   AGE
podinfo   1/1     1            1           2d4h
```
### 4 - OBSERVALIDADE

Prometheus Stack (prometheus, grafana, alertmanager) com retenção de 3 dias e storage local (disco), persistente.

```
angela@angela:~/Desktop/DesafioDevOps$ kubectl apply -f persistent_volume.yaml
persistentvolume/prometheus-pv created
```
```
angela@angela:~/Desktop/DesafioDevOps$ helm install -f values.yaml prometheus prometheus-community/kube-prometheus-stack 
NAME: prometheus
LAST DEPLOYED: Tue Nov 15 20:06:06 2022
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
kube-prometheus-stack has been installed. Check its status by running:
  kubectl --namespace default get pods -l "release=prometheus"

Visit https://github.com/prometheus-operator/kube-prometheus for instructions on how to create & configure Alertmanager and Prometheus instances using the Operator.
```
```
angela@angela:~/Desktop/DesafioDevOps$ kubectl get pods
NAME                                                     READY   STATUS    RESTARTS      AGE
alertmanager-prometheus-kube-prometheus-alertmanager-0   2/2     Running   1 (24m ago)   24m
loki-0                                                   1/1     Running   0             32h
loki-promtail-7ftx2                                      1/1     Running   0             32h
loki-promtail-7w2x5                                      1/1     Running   0             32h
podinfo-6cf8996bcd-xwcrz                                 1/1     Running   0             49m
prometheus-grafana-577567fff9-fnnm9                      3/3     Running   0             24m
prometheus-kube-prometheus-operator-67b9b6866c-99bsj     1/1     Running   0             24m
prometheus-kube-state-metrics-56b85865-sqmhv             1/1     Running   0             24m
prometheus-prometheus-kube-prometheus-prometheus-0       2/2     Running   0             24m
prometheus-prometheus-node-exporter-22qc6                1/1     Running   0             24m
prometheus-prometheus-node-exporter-tf4pg                1/1     Running   0             24m
```
```
angela@angela:~/Desktop/DesafioDevOps$ kubectl get deployments
NAME                                  READY   UP-TO-DATE   AVAILABLE   AGE
podinfo                               1/1     1            1           2d5h
prometheus-grafana                    1/1     1            1           25m
prometheus-kube-prometheus-operator   1/1     1            1           25m
prometheus-kube-state-metrics         1/1     1            1           25m
```
```
angela@angela:~/Desktop/DesafioDevOps$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                                                                                                            STORAGECLASS       REASON   AGE
prometheus-pv                              50Gi       RWO            Retain           Bound    default/prometheus-prometheus-kube-prometheus-prometheus-db-prometheus-prometheus-kube-prometheus-prometheus-0   prometheusmanual            18m
pvc-3e9a00d1-889b-416d-a0e2-03b26a716ea1   10Gi       RWO            Delete           Bound    default/prometheus-grafana                                                                                       standard                    17m
```

```
angela@angela:~/Desktop/DesafioDevOps$ kubectl get pvc
NAME                                                                                                     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
prometheus-grafana                                                                                       Bound    pvc-3e9a00d1-889b-416d-a0e2-03b26a716ea1   10Gi       RWO            standard           16m
prometheus-prometheus-kube-prometheus-prometheus-db-prometheus-prometheus-kube-prometheus-prometheus-0   Bound    prometheus-pv                              50Gi       RWO            prometheusmanual   16m
angela@angela:~/Desktop/DesafioDevOps$ 
```
##### Arquivos para o Persistent Volume e instalação do Prometheus Stack:

[Persistent Volume](https://gitlab.com/angela.poliana/DesafioDevOps/-/blob/master/persistent_volume.yaml)<br>
[Prometheus Stack](https://gitlab.com/angela.poliana/DesafioDevOps/-/blob/master/values.yaml)<br>

##### Retenção de métricas 3 dias
```
angela@angela:~/Desktop/DesafioDevOps$ kubectl describe pods prometheus-prometheus-kube-prometheus-prometheus-0 | grep retentio
      --storage.tsdb.retention.time=3d
```
##### Painel Grafana
![prep07](Imagens/7.png)<br>
![prep08](Imagens/8.png)<br>

##### Prometheus
![prep09](Imagens/9.png)<br>

##### Logs centralizados (Loki)
Instalação do Loki com o Helm:<br>
![prep03](Imagens/2.png)<br>

Logs:
![prep04](Imagens/3.png)<br>

##### Alertas para o Canal do Telegram
![prep05](Imagens/4.jpeg)<br>
![prep06](Imagens/5.jpeg)<br>